<?php
namespace Parasut;

class Category extends Base
{

    public function create($data)
    {
        return $this->client->request(
            'item_categories',
            $data
        );
    }

    public function show($id , $data = [])
    {
        return $this->client->request(
            'item_categories/' . $id,
            $data,
            'GET'
        );
    }

    public function update($id , $data = [])
    {
        return $this->client->request(
            'item_categories/' . $id,
            $data,
            'PUT'
        );
    }
}