<?php
namespace Parasut;

class Tag extends Base
{

    public function create($data)
    {
        return $this->client->request(
            'tags',
            $data
        );
    }

    public function show($id , $data = [])
    {
        return $this->client->request(
            'tags/' . $id,
            $data,
            'GET'
        );
    }

    public function update($id , $data = [])
    {
        return $this->client->request(
            'tags/' . $id,
            $data,
            'PUT'
        );
    }
}